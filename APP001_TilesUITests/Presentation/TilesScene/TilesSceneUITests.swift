//
//  TilesSceneUITests.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import XCTest

class TilesSceneUITests: XCTestCase {
    override func setUp() {
        continueAfterFailure = false
        XCUIApplication().launch()
    }

    // NOTE: for UI tests to work the keyboard of simulator must be on.
    // Keyboard shortcut COMMAND + SHIFT + K while simulator has focus
    func testOpenTiles_whenTapOnItem_thenTileMessageShows() {
        let app = XCUIApplication()
        app.navigationBars.firstMatch.buttons["Refresh"].tap()
        app.buttons.element(boundBy: 1).tap()
        let elementsQuery = app.alerts["Message"].scrollViews.otherElements.buttons["OK"]
        _ = elementsQuery.waitForExistence(timeout: 5)
        elementsQuery.tap()
    }
}
