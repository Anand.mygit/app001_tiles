//
//  DefaultMessageQueryRepository.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation
final class DefaultMessageRepository {
    private let dataTransferService: DataTransferService

    init(dataTransferService: DataTransferService) {
        self.dataTransferService = dataTransferService
    }
}

extension DefaultMessageRepository: MessageQueryRepository {
    func fetchTileMessage(query: TileQuery, cached _: @escaping (String) -> Void, completion: @escaping (Result<String, Error>) -> Void) -> Cancellable? {
        let id = query.query.replacingOccurrences(of: "\\", with: "")
        let requestDTO = MessageRequestDTO(id: id)
        let task = RepositoryTask()

        guard !task.isCancelled else { return nil }

        let endpoint = APIEndpoints.postSelection(with: requestDTO)
        task.networkTask = dataTransferService.request(with: endpoint) { result in
//                debugPrint(result)
            switch result {
            case let .success(responseDTO):
                completion(.success(responseDTO.message ?? ""))
            case let .failure(error):
                completion(.failure(error))
            }
        }
        return task
    }
}
