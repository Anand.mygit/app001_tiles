//
//  DefaultTilesRepository.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

final class DefaultTilesRepository {
    private let dataTransferService: DataTransferService

    init(dataTransferService: DataTransferService) {
        self.dataTransferService = dataTransferService
    }
}

extension DefaultTilesRepository: TilesRepository {
    public func fetchTilesList(cached _: @escaping ([Tile]) -> Void,
                               completion: @escaping (Result<[Tile], Error>) -> Void) -> Cancellable? {
        let requestDTO = TilesRequestDTO()
        let task = RepositoryTask()

        guard !task.isCancelled else { return nil }

        let endpoint = APIEndpoints.getTiles(with: requestDTO)
        task.networkTask = dataTransferService.request(with: endpoint) { result in
            switch result {
            case let .success(responseDTO):
                printIfDebug("Tiles Count: \(responseDTO.count) ")
//                    printIfDebug("Tiles: \(responseDTO) ") // Too much logs
                completion(.success(responseDTO.map { $0.mapToDomain() }))
            case let .failure(error):
                completion(.failure(error))
            }
        }
        return task
    }
}
