//
//  TilesResponseDTO+Mapping.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//
import Foundation

// MARK: - Data Transfer Object

struct TilesResponseDTO: Decodable {
    var id: String?
    var label: String?
    var priority: Double?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case label = "Label"
        case priority = "Priority"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        label = try values.decodeIfPresent(String.self, forKey: .label)
        priority = try values.decodeIfPresent(Double.self, forKey: .priority)
    }
}

extension TilesResponseDTO {
    struct TileDTO: Decodable {
        private enum CodingKeys: String, CodingKey {
            case id = "Id"
            case label = "Label"
            case priority = "Priority"
        }

        var id: String?
        var label: String?
        var priority: Double?
    }
}

// MARK: - Mappings to Domain

extension TilesResponseDTO {
    func mapToDomain() -> Tile {
        return Tile(id: id ?? "", label: label, priority: priority)
    }
}

// MARK: - Private
