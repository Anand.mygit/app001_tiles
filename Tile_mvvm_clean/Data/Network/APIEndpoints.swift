//
//  APIEndpoints.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

struct APIEndpoints {
    static func getTiles(with TilesRequestDTO: TilesRequestDTO) -> Endpoint<[TilesResponseDTO]> {
        return Endpoint(path: "tiles",
                        method: .get,
                        queryParametersEncodable: TilesRequestDTO)
    }

    static func postSelection(with TilesRequestDTO: MessageRequestDTO) -> Endpoint<MessageResponseDTO> {
        return Endpoint(path: "selection",
                        method: .post,
                        queryParametersEncodable: TilesRequestDTO)
    }
}
