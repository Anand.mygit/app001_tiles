//
//  TilesSceneDIContainer.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import SwiftUI
import UIKit

final class TilesSceneDIContainer {
    struct Dependencies {
        let apiDataTransferService: DataTransferService
    }

    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }

    // MARK: - Use Cases

    func makeDisplayTilesUseCase() -> DisplayTilesUseCase {
        return DefaultDisplayTilesUseCase(TilesRepository: makeTilesRepository())
    }

    func makeDisplyMessageUseCase() -> DisplayMessageUseCase {
        return DefaultDisplayMessageUseCase(tilesRepository: makeMessageRepository())
    }

    // MARK: - Repositories

    func makeTilesRepository() -> TilesRepository {
        return DefaultTilesRepository(dataTransferService: dependencies.apiDataTransferService)
    }

    func makeMessageRepository() -> MessageQueryRepository {
        return DefaultMessageRepository(dataTransferService: dependencies.apiDataTransferService)
    }

    // MARK: - Tiles List

    func makeTilesViewController(closures: TilesViewModelClosures) -> TilesViewController {
        return TilesViewController.create(with: makeTilesViewModel(closures: closures))
    }

    func makeTilesViewModel(closures: TilesViewModelClosures) -> TilesViewModel {
        return DefaultTilesViewModel(displayTilesUseCase: makeDisplayTilesUseCase(), displayMessageUseCase: makeDisplyMessageUseCase(),
                                     closures: closures)
    }

    // MARK: - Flow Coordinators

    func makeTilesDisplayFlowCoordinator(navigationController: UINavigationController) -> TilesDisplayFlowCoordinator {
        return TilesDisplayFlowCoordinator(navigationController: navigationController,
                                           dependencies: self)
    }
}

extension TilesSceneDIContainer: TilesDisplayFlowCoordinatorDependencies {}
