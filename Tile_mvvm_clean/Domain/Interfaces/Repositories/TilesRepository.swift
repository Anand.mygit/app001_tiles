//
//  TilesRepositoryInterfaces.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

protocol TilesRepository {
    @discardableResult
    func fetchTilesList(cached: @escaping ([Tile]) -> Void,
                        completion: @escaping (Result<[Tile], Error>) -> Void) -> Cancellable?
}
