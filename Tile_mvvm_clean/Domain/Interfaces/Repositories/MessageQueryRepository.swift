//
//  MessageQueryRepository.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation
protocol MessageQueryRepository {
    func fetchTileMessage(query: TileQuery,
                          cached: @escaping (String) -> Void,
                          completion: @escaping (Result<String, Error>) -> Void) -> Cancellable?
}
