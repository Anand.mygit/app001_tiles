//
//  DisplayMessageUseCase.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

protocol DisplayMessageUseCase {
    func execute(requestValue: DisplayMessageUseCaseRequestValue,
                 cached: @escaping (String) -> Void,
                 completion: @escaping (Result<String, Error>) -> Void) -> Cancellable?
}

final class DefaultDisplayMessageUseCase: DisplayMessageUseCase {
    private let tilesRepository: MessageQueryRepository

    init(tilesRepository: MessageQueryRepository) {
        self.tilesRepository = tilesRepository
    }

    func execute(requestValue: DisplayMessageUseCaseRequestValue, cached: @escaping (String) -> Void, completion: @escaping (Result<String, Error>) -> Void) -> Cancellable? {
        return tilesRepository.fetchTileMessage(query: requestValue.query,
                                                cached: cached,
                                                completion: { result in

                                                    if case .success = result {}

                                                    completion(result)
        })
    }
}

struct DisplayMessageUseCaseRequestValue {
    let query: TileQuery
}
