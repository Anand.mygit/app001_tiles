//
//  DisplayTilesUseCase.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

protocol DisplayTilesUseCase {
    func execute(requestValue: DisplayTilesUseCaseRequestValue,
                 cached: @escaping ([Tile]) -> Void,
                 completion: @escaping (Result<[Tile], Error>) -> Void) -> Cancellable?
}

final class DefaultDisplayTilesUseCase: DisplayTilesUseCase {
    private let tilesRepository: TilesRepository

    init(TilesRepository: TilesRepository) {
        tilesRepository = TilesRepository
    }

    func execute(requestValue _: DisplayTilesUseCaseRequestValue,
                 cached: @escaping ([Tile]) -> Void,
                 completion: @escaping (Result<[Tile], Error>) -> Void) -> Cancellable? {
        return tilesRepository.fetchTilesList(cached: cached,
                                              completion: { result in

                                                  if case .success = result {}

                                                  completion(result)
        })
    }
}

struct DisplayTilesUseCaseRequestValue {}
