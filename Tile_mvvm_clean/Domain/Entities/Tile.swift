//
//  Tile.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

typealias TileId = String

struct Tile: Equatable, Identifiable {
    let id: TileId
    let label: String?
    let priority: Double?
}
