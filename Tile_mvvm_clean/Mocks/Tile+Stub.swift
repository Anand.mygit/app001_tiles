//
//  Tile+Stub.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation
@testable import APP001_Tiles

extension Tile {
    static func stub(id: TileId = "id1",
                     title: String = "title1",
                     priority: Double? = 0.9) -> Self {
        Tile(id: id, label: title, priority: priority)
    }
}
