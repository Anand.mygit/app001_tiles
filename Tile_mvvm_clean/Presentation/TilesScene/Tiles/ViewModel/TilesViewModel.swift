//
//  TilesListViewModel.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

struct TilesViewModelClosures {
    let showTileDetails: (String) -> Void
}

enum TilesViewModelLoading {
    case fullScreen
    case nextPage
}

protocol TilesListViewModelInput {
    func viewDidLoad()
    func didRefresh()
    func didSelect(item: TilesItemViewModel)
}

protocol TilesViewModelOutput {
    var items: Observable<[TilesItemViewModel]> { get }
    var loadingType: Observable<TilesViewModelLoading?> { get }
    var messageOnSelect: Observable<String> { get }
    var error: Observable<String> { get }
    var isEmpty: Bool { get }
    var screenTitle: String { get }
    var emptyDataTitle: String { get }
    var errorTitle: String { get }
}

protocol TilesViewModel: TilesListViewModelInput, TilesViewModelOutput {}

final class DefaultTilesViewModel: TilesViewModel {
    private let displayTilesUseCase: DisplayTilesUseCase
    private let closures: TilesViewModelClosures?

    private let displayMessageUseCase: DisplayMessageUseCase
    private var tilesLoadTask: Cancellable? { willSet { tilesLoadTask?.cancel() } }

    // MARK: - OUTPUT

    let items: Observable<[TilesItemViewModel]> = Observable([])
    let loadingType: Observable<TilesViewModelLoading?> = Observable(.none)
    let messageOnSelect: Observable<String> = Observable("")
    let error: Observable<String> = Observable("")
    var isEmpty: Bool { return items.value.isEmpty }
    let screenTitle = NSLocalizedString("Pick Your Tile", comment: "")
    let emptyDataTitle = NSLocalizedString("No tiles", comment: "")
    let errorTitle = NSLocalizedString("Error", comment: "")
    let displayBarPlaceholder = NSLocalizedString("Display Tiles", comment: "")

    init(displayTilesUseCase: DisplayTilesUseCase, displayMessageUseCase: DisplayMessageUseCase,
         closures: TilesViewModelClosures? = nil) {
        self.displayTilesUseCase = displayTilesUseCase
        self.closures = closures
        self.displayMessageUseCase = displayMessageUseCase
    }

    private func reloadTiles(_ tiles: [Tile]) {
        let sortedTiles = tiles.sorted { // Sort based on priority
            ($0.priority ?? 0.0) < ($1.priority ?? 0.0)
        }
        items.value = sortedTiles.compactMap { TilesItemViewModel(tile: $0) }
    }

    private func resetPages() {
        items.value.removeAll()
    }

    private func load(loadingType: TilesViewModelLoading) {
        self.loadingType.value = loadingType

        tilesLoadTask = displayTilesUseCase.execute(
            requestValue: .init(),
            cached: { page in
                self.reloadTiles(page)
            },
            completion: { result in
                switch result {
                case let .success(page):
                    self.reloadTiles(page)

                case let .failure(error):
                    self.handle(error: error)
                }
                self.loadingType.value = .none
            }
        )
    }

    private func send(TileQuery: TileQuery, loadingType: TilesViewModelLoading) {
        self.loadingType.value = loadingType

        tilesLoadTask = displayMessageUseCase.execute(
            requestValue: .init(query: TileQuery),
            cached: { _ in
            },
            completion: { result in
                switch result {
                case let .success(message):
                    self.messageOnSelect.value = message
                    self.closures?.showTileDetails(message)

                case let .failure(error):
                    self.handle(error: error)
                }
                self.loadingType.value = .none
            }
        )
    }

    private func handle(error: Error) {
        self.error.value = error.isInternetConnectionError ?
            NSLocalizedString("No internet connection", comment: "") :
            NSLocalizedString("Failed loading Tiles", comment: "")
    }

    private func update() {
        resetPages()
        load(loadingType: .fullScreen)
    }

    private func getDetail(id: String?) {
        let tileQuery = TileQuery(query: id ?? "")
        send(TileQuery: tileQuery, loadingType: .fullScreen)
    }
}

// MARK: - INPUT. View event methods

extension DefaultTilesViewModel {
    func viewDidLoad() {
        update()
    }

    func didRefresh() {
        update()
    }

    func didSelect(item: TilesItemViewModel) {
        getDetail(id: item.id)
    }
}
