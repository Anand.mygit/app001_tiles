//
//  TilesListItemViewModel.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import Foundation

struct TilesItemViewModel: Equatable {
    var title: String?
    var id: String?
    var priority: Double?
}

extension TilesItemViewModel {
    init(tile: Tile) {
        id = tile.id
        title = tile.label ?? ""
        priority = tile.priority
    }
}
