//
//  TilesListViewController.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import UIKit

final class TilesViewController: UIViewController, StoryboardInstantiable, Alertable {
    @IBOutlet private var contentView: UIView!
    @IBOutlet private var emptyDataLabel: UILabel!
    @IBOutlet var tileView: GridView!

    private var viewModel: TilesViewModel!

    static func create(with viewModel: TilesViewModel) -> TilesViewController {
        let view = TilesViewController.instantiateViewController()
        view.viewModel = viewModel
        return view
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = viewModel.screenTitle
        emptyDataLabel.text = viewModel.emptyDataTitle
        setupRefreshButton()

        bind(to: viewModel)
        viewModel.viewDidLoad()
    }

    private func bind(to viewModel: TilesViewModel) {
        viewModel.items.observe(on: self) { [weak self] in
            self?.showTiles(tilesListItemViewModels: $0)
        }

        viewModel.messageOnSelect.observe(on: self) { [weak self] in self?.showMessage($0) }
        viewModel.error.observe(on: self) { [weak self] in self?.showError($0) }
        viewModel.loadingType.observe(on: self) { [weak self] _ in self?.updateViewsVisibility() }
    }

    private func showError(_ error: String) {
        guard !error.isEmpty else { return }
        showAlert(title: viewModel.errorTitle, message: error)
    }

    private func showMessage(_ value: String) {
        guard !value.isEmpty else { return }
        showAlert(title: "Message", message: value)
    }

    private func showTiles(tilesListItemViewModels: [TilesItemViewModel]) {
        tileView.removeAllCell()

        for item in tilesListItemViewModels {
            let button = TileView(frame: .zero)
            button.id = item.title
            button.setTitle(item.title, for: .normal)
            button.addTarget(self, action: #selector(tileTapped), for: UIControl.Event.touchUpInside)
            tileView.addCell(view: button)
        }
    }

    @objc func tileTapped(sender: TileView) {
        if let item = viewModel.items.value.filter({ $0.title == sender.id }).first {
            viewModel.didSelect(item: item)
        }
    }

    private func updateViewsVisibility() {
        emptyDataLabel.isHidden = true
        LoadingView.hide()

        switch viewModel.loadingType.value {
        case .fullScreen: LoadingView.show()
        case .nextPage: break
        case .none:
            break
        }
    }

    @objc func reloadButtonClicked() {
        viewModel.didRefresh()
    }
}

extension TilesViewController {
    private func setupRefreshButton() {
        let add = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(reloadButtonClicked))
        navigationItem.rightBarButtonItem = add
    }
}
