//
//  TilesDisplayFlowCoordinator.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import UIKit

protocol TilesDisplayFlowCoordinatorDependencies {
    func makeTilesViewController(closures: TilesViewModelClosures) -> TilesViewController
}

class TilesDisplayFlowCoordinator {
    private let navigationController: UINavigationController
    private let dependencies: TilesDisplayFlowCoordinatorDependencies

    private weak var tilesListVC: TilesViewController?

    init(navigationController: UINavigationController,
         dependencies: TilesDisplayFlowCoordinatorDependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }

    func start() {
        // Note: here we keep strong reference with closures, this way this flow do not need to be strong referenced
        let closures = TilesViewModelClosures(showTileDetails: showTileDetails)
        let vc = dependencies.makeTilesViewController(closures: closures)

        navigationController.pushViewController(vc, animated: false)
        tilesListVC = vc
    }

    private func showTileDetails(Tile _: String) {}
}
