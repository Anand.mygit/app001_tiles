//
//  DisplayTilesUseCaseTests.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//

import XCTest
@testable import APP001_Tiles

class DisplayTilesUseCaseTests: XCTestCase {
    static let mockTiles: [Tile] = {
        let page1 = Tile(id: "Tile1", label: "Tile1", priority: 0.9)
        let page2 = Tile(id: "Tile1", label: "Tile1", priority: 0.9)
        return [page1, page2]
    }()

    enum TilesRepositorySuccessTestError: Error {
        case failedFetching
    }

    struct TilesRepositoryMock: TilesRepository {
        var result: Result<[Tile], Error>

        func fetchTilesList(cached _: @escaping ([Tile]) -> Void, completion: @escaping (Result<[Tile], Error>) -> Void) -> Cancellable? {
            completion(result)
            return nil
        }
    }

    func test_DisplayTilesUseCase_whenSuccessfullyFetchesTiles_thenTilesCountGreaterThanZero() {
        // given
        let expectation = self.expectation(description: "Recent query saved")
//        expectation.expectedFulfillmentCount = 2
        var tiles: [Tile] = []
        let tilesRepositoryMock = TilesRepositoryMock(result: .success(DisplayTilesUseCaseTests.mockTiles))

        let useCase = DefaultDisplayTilesUseCase(TilesRepository: tilesRepositoryMock)

        // when
        let requestValue = DisplayTilesUseCaseRequestValue()
        _ = useCase.execute(requestValue: requestValue, cached: { _ in }) { result in
            switch result {
            case let .success(value):
                tiles = value
            case let .failure(error):
                debugPrint(error)
            }
            expectation.fulfill()
        }

        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertTrue(tiles.count == DisplayTilesUseCaseTests.mockTiles.count)
    }

    func test_DisplayTilesUseCase_whenFailedFetchingTiles_thenQueryCountIsZero() {
        // given
        let expectation = self.expectation(description: "Recent query should not be saved")
//        expectation.expectedFulfillmentCount = 2
        var tiles: [Tile] = []
        let tilesRepositoryMock = TilesRepositoryMock(result: .failure(TilesRepositorySuccessTestError.failedFetching))

        let useCase = DefaultDisplayTilesUseCase(TilesRepository: tilesRepositoryMock)

        // when
        let requestValue = DisplayTilesUseCaseRequestValue()
        _ = useCase.execute(requestValue: requestValue, cached: { _ in }) { result in
            switch result {
            case let .success(value):
                tiles = value
            case let .failure(error):
                debugPrint(error)
            }
            expectation.fulfill()
        }
        // then

        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertTrue(tiles.isEmpty)
    }
}
