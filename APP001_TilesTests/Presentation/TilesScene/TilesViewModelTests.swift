//
//  TilesListViewModelTests.swift
//  mvvm-clean-architecture
//
//  Created by Anand, Chetan on 12/4/20.
//  Copyright © 2020 Anand, Chetan. All rights reserved.
//
import XCTest
@testable import APP001_Tiles

class TilesListViewModelTests: XCTestCase {
    private enum DisplayTilesUseCaseError: Error {
        case someError
    }

    static let mockTiles: [Tile] = {
        let page1 = Tile(id: "Tile1", label: "Tile1", priority: 0.9)
        let page2 = Tile(id: "Tile1", label: "Tile1", priority: 0.1)
        return [page1, page2]
    }()

    class DisplayTilesUseCaseMock: DisplayTilesUseCase {
        var expectation: XCTestExpectation?
        var error: Error?

        func execute(requestValue _: DisplayTilesUseCaseRequestValue,
                     cached _: @escaping ([Tile]) -> Void,
                     completion: @escaping (Result<[Tile], Error>) -> Void) -> Cancellable? {
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(mockTiles))
            }
            expectation?.fulfill()
            return nil
        }
    }

    class DisplayMessageUseCaseMock: DisplayMessageUseCase {
        var expectation: XCTestExpectation?
        var error: Error?
        var message = "Message Received"

        func execute(requestValue _: DisplayMessageUseCaseRequestValue, cached _: @escaping (String) -> Void, completion: @escaping (Result<String, Error>) -> Void) -> Cancellable? {
            if let error = error {
                completion(.failure(error))
            } else {
                completion(.success(message))
            }
            expectation?.fulfill()
            return nil
        }
    }

    func test_whenDisplayTilesUseCaseRetrievesTilesList_thenViewModelContainsTilesList() {
        // given
        let displayTilesUseCaseMock = DisplayTilesUseCaseMock()
        let displayMessageUseCaseMock = DisplayMessageUseCaseMock()

        displayTilesUseCaseMock.expectation = expectation(description: "Displays tiles")
        let viewModel = DefaultTilesViewModel(displayTilesUseCase: displayTilesUseCaseMock, displayMessageUseCase: displayMessageUseCaseMock)
        // when
        viewModel.didRefresh()

        // then
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertTrue(viewModel.items.value.count > 0)
    }
}
